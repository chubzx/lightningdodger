#!/usr/local/bin/python

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)

sensorPin = 7
pulsePin = 11
flashLightLevel = 1000
flashCount = 0

def getLightValue (sensorPin):
    count = 0
  
    GPIO.setup(sensorPin, GPIO.OUT)
    GPIO.output(sensorPin, GPIO.LOW)
    time.sleep(0.1)

    GPIO.setup(sensorPin, GPIO.IN)
  
    while (GPIO.input(sensorPin) == GPIO.LOW):
        count += 1

    return count

def checkForFlash (lightLevel):
    GPIO.setup(pulsePin, GPIO.OUT)
    if lightLevel < flashLightLevel:
        GPIO.output(pulsePin, GPIO.HIGH)
        
        time.sleep(.500)
        global flashCount
        flashCount += 1
        print(flashCount)

        GPIO.output(pulsePin, GPIO.LOW)
    else:
        GPIO.output(pulsePin, GPIO.LOW)

try:
    while True:
        checkForFlash(getLightValue(sensorPin))
except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()